import features as f
import pca as p
import wCloud as cloud
from sklearn.cluster import KMeans 
from matplotlib import pyplot as plt
from scipy import sparse
from json import dumps, loads
import numpy as np
from scipy.spatial.distance import cdist, pdist
from sklearn.metrics import silhouette_score
from sklearn.metrics import euclidean_distances
from variance import variancePerCluster
from sys import argv

def loadFullKMeans(currentRun, labelAtual,cor):
    global indexes
    inertias = np.load("finalRunInertia" + currentRun + ".npy")
    indexes = np.load(currentRun + "-indexes.npy")
    KMs = np.load("finalRunKM" + currentRun + ".npy")
    #plt.figure()
    ax.plot(range(2, 30), inertias,'b*-',label=labelAtual,color=cor)
    
    return inertias, KMs



def _cluster_variance(num_points, clusters, centroids):
    '''
    usar essa função pra obter variancia, não testei ainda
    '''
    s = 0
    denom = float(num_points - len(centroids))
    for cluster, centroid in zip(clusters, centroids):
        distances = euclidean_distances(cluster, centroid)
        s += (distances*distances).sum()
    return s / denom


def loadAndPlot(currentRun, labelAtual, cor):
    dataMatriz=[]
    try:
        dataMatrix = np.load(currentRun + "-data.npy")
        plotData(*dataMatrix, labelAtual,cor)
    except FileNotFoundError:
        print("Data not found for run " + currentRun + " maybe run kmeans for that data configuration?")
        quit()
    return (dataMatrix)

def plotData(K, criterion, variance, s, seg_gains, seg_threshold, KM, labelAtual, cor):
    #Get the first index satisfying the threshold
    #kIdx = np.argmax(seg_gains > seg_threshold)

    # elbow curve
    ax.plot(K, criterion, 'b*-', label=labelAtual,color=cor)
    # ax.plot(K[kIdx], criterion[kIdx], marker='o', markersize=12, markeredgewidth=2, markeredgecolor='r', markerfacecolor='None')

    #Shilhouette
    ax2.plot(K, s, 'b*-',label=labelAtual,color=cor)


def clouds(predicts,k):

    for j in range(0,k):
        data = []
        for i in range(0,len(predicts)):
            if(j==predicts[i]):
                data.append(indexes[i])
        cloud.generateWordCloud(data,currentRun+'-wordCloud'+str(j))
    plt.show()



# Main
if __name__ == '__main__':
    costs = {}
    tipo = 'word'
    nsamples = 0.5
    Y = 2
    M = None
    n = (3,3)
    
    k= 14
    
    currentRun=''
    colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
    fig, ax = plt.subplots(figsize=(8, 5))
    fig2, ax2 = plt.subplots(figsize=(8, 5))
    fig3, ax3 = plt.subplots(figsize=(8, 5))
    cor = 0

    for n in [(3,3)]:
        legenda = str(n[0]) + "gram"
        if n == (4,4):
            nsamples = 0.1
            tipo = 'char'
            legenda = str(n[0]) + "char"
        if n == (2,3):
            legenda = str(n[0]) + str(n[1]) + "gram"
        
        currentRun = "-" + str(nsamples) + "samples" + str(n[0]) +  str(n[1]) + str(tipo) + "Max" + str(M) + "year" + str(Y)
        features = sparse.load_npz(currentRun + "-features.npz")
        name=currentRun
        

        inertias, KMs = loadFullKMeans(currentRun, legenda,colors[cor%10])
        #K, criterion, variance, s, seg_gains, seg_threshold, KMs = loadAndPlot(currentRun, legenda,colors[cor%10])
        #varicance per cluster
        '''
        features = features.toarray().astype(np.float16)
        variance = variancePerCluster((KMs[k - 2]).labels_, features, (KMs[k - 2]).cluster_centers_)
        ax3.plot(range(2, k + 2), variance, 'b*-', label = legenda,color=colors[cor%10])
        '''
        cor+=1
        #distribution of clusters
        for i in [k]:
            fig4, ax4 = plt.subplots(figsize=(8, 5))
            print(ax4.hist(KMs[i - 2].labels_, color = 'r')[0]/len(indexes))   
            ax4.grid(True)
            ax4.set_title('Distribution for k= '+str(k))           
            ax4.set_ylabel("Number of headlines")
            ax4.set_xlabel("Cluster")
            fig4.savefig(name+str(M)+'Histogram'+str(k)+'-Graphic.png') 
            fig4.show()
        
        #word clouds
        #clouds(KMs[k - 2].labels_, k)

        

    '''ax.grid(True)
    ax.legend(loc='right')
    ax.set_xlabel('Number of clusters')
    ax.set_ylabel('Average within-cluster sum of squares')
    ax.set_title('Average distance')
    fig.savefig(name+'Graphic.png') 
    fig.show()'''


    ax.grid(True)
    ax.legend(loc='right')
    ax.set_xlabel('Number of clusters')
    ax.set_ylabel('Average within-cluster sum of squared distances')
    ax.set_title('Average distance')
    fig.savefig(name+'dist-Graphic.png') 
    fig.show()
    '''
    ax2.set_ylabel("Silouette Coefficient")
    ax2.grid(True)
    ax2.legend(loc='right')
    ax2.set_xlabel("Number of clusters")
    ax2.set_title("Silouette")
    fig2.savefig(name+'silhouette-Graphic.png') 
    fig2.show()
    '''
    
    
    ax3.set_title("Variance per Cluster" + str(k))
    ax3.set_ylabel("Variance")
    ax3.grid(True)
    ax3.legend(loc='right')
    ax3.set_xlabel("Cluster")
    fig3.savefig(name+'Variance-Graphic.png') 
    fig3.show()