
import features as f
import pca as p
import wCloud as cloud
from sklearn.cluster import KMeans 
from matplotlib import pyplot as plt
from scipy import sparse
from json import dumps, loads
import numpy as np
from scipy.spatial.distance import cdist, pdist
from sklearn.metrics import silhouette_score
from sklearn.metrics import euclidean_distances
from variance import variancePerCluster
from sys import argv

'''n = 3
tipo = 'word'
M = 5000
Y = 2
nsamples = 1
currentRun = "-" + str(nsamples) + "samples" + str(n[0]) +  str(n[1]) + str(tipo) + "Max" + str(M) + "year" + str(Y)


if __name__ == '__main__':
    try:
        features = sparse.load_npz(currentRun + "-features.npz")
        indexes = np.load(currentRun + "-indexes.npy")
        print("Loaded features and indexes!")
    except IOError:
        print("Feature not found, calculating...")
        features, indexes = f.init(n, tipo=tipo, M=M, Y=Y, num_samples=nsamples) 
        sparse.save_npz(currentRun + "-features.npz", features)
        np.save(currentRun + "-indexes.npy", indexes)
        print("Features and indexes saved!")
'''
# Final Kmeans pure run
def kMeans(KRange):
    K = KRange
    inertias = []
    KMs = []
    for k in K:
        print(currentRun + " k: " + str(k))
        KM = KMeans(n_clusters=k, n_jobs=-1).fit(features)
        inertias.append(KM.inertia_)
        KMs.append(KM)        
        #variancePerCluster(KM.labels_, features.toarray().astype(np.float16), KM.cluster_centers_)  
    inertias = np.array(inertias) / features.shape[0]
    np.save("finalRunInertia" + currentRun + ".npy", inertias)
    np.save("finalRunKM" + currentRun + ".npy", np.asarray(KMs))
    plt.plot(K, inertias.tolist(), 'b*-')

def loadFullKMeans(currentRun):
    global indexes
    inertias = np.load("finalRunInertia" + currentRun + ".npy")
    indexes = np.load(currentRun + "-indexes.npy")
    KMs = np.load("finalRunKM" + currentRun + ".npy")
    features = sparse.load_npz(currentRun + "-features.npz")
    plt.plot(range(2, 30), inertias, 'b*-')
    plt.show()
    for k in [7]:
        plt.figure()
        plt.title(str(k))
        print(plt.hist(KMs[k - 2].labels_)[0]/len(indexes))
        clouds(KMs[k - 2].labels_, k)
    plt.show()
    return inertias, KMs


def printCosts():
    print("Final costs: ")
    K = []
    cost = []

    # Worst possible way to sort a dict by me
    for k, c in costs.items():
        print(str(k) + str(c))
        K.append(k)
    K = sorted(K)
    for i in K:
        for k, c in costs.items():
            if k == i:
                cost.append(c)

    tosave = [K, cost]
    mink = np.min(K)
    maxk = np.max(K)
    with open(currentRun + "kfrom" + str(mink) + "to" + str(maxk) + "_costs.txt", 'w') as file:
        file.write(dumps(tosave))
    plt.plot(K, cost)
    plt.show()

def loadCosts(mink, maxk, currentRun):
    with open(currentRun + "kfrom" + str(mink) + "to" + str(maxk) + "_costs.txt", 'r') as file:
        return loads(file.read())

#The angle between three points
def segments_gain(p1, v, p2):
    vp1 = np.linalg.norm(p1 - v)
    vp2 = np.linalg.norm(p2 - v)
    p1p2 = np.linalg.norm(p1 - p2)
    return np.arccos((vp1**2 + vp2**2 - p1p2**2) / (2 * vp1 * vp2)) / np.pi


def _cluster_variance(num_points, clusters, centroids):
    '''
    usar essa função pra obter variancia, não testei ainda
    '''
    s = 0
    denom = float(num_points - len(centroids))
    for cluster, centroid in zip(clusters, centroids):
        distances = euclidean_distances(cluster, centroid)
        s += (distances*distances).sum()
    return s / denom



def kmeansTest(dt_trans):
    seg_threshold = 0.95 #Set this to your desired target
    K = range(10,11)
    s = [] #silhouette
    KM = []
    for k in K:
        kM = KMeans(n_clusters=k,n_jobs=-2).fit(dt_trans)
        labels = kM.labels_
        centroids = kM.cluster_centers_
        KM.append(kM)
        s.append(silhouette_score(dt_trans, labels, metric='euclidean')) 
    centroids = [k.cluster_centers_ for k in KM]
    data = dt_trans.toarray().astype(np.float16)
    D_k = [cdist(data, cent, 'euclidean') for cent in centroids]
    cIdx = [np.argmin(D,axis=1) for D in D_k]
    dist = [np.min(D,axis=1) for D in D_k]
    avgWithinSS = [sum(d)/dt_trans.shape[0] for d in dist]

    clouds(KM[15].labels_,16)

     # Total with-in sum of square
    wcss = [sum(d**2) for d in dist]
    tss = sum(pdist(data)**2)/data.shape[0]
    bss = tss-wcss

    #Normalize the data
    criterion = np.array(avgWithinSS)
    criterion = (criterion - criterion.min()) / (criterion.max() - criterion.min())

    #Compute the angles
    seg_gains = np.array([0, ] + [segments_gain(*[np.array([K[j], criterion[j]]) for j in range(i-1, i+2)]) for i in range(len(K) - 2)] + [np.nan, ])


    variance = [np.var(d) for d in dist]

    #Save new data
    dataMatrix = np.asarray([K, criterion, variance, s, seg_gains, seg_threshold, KM])
    np.save(currentRun + "-data.npy", dataMatrix)
    print("Plots saved.")

    plotData(K, criterion, variance,bss/tss, s, seg_gains, seg_threshold, None)

def loadAndPlot(currentRun):
    try:
        dataMatrix = np.load(currentRun + "-data.npy")
        plotData(*dataMatrix)
    except FileNotFoundError:
        print("Data not found for run " + currentRun + " maybe run kmeans for that data configuration?")
        quit()

def plotData(K, criterion, variance, explained_var, s, seg_gains, seg_threshold, KM):
    

    #Do whatever you want with kM, it has centroids and labels

    #Get the first index satisfying the threshold
    kIdx = np.argmax(seg_gains > seg_threshold)


    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(K, explained_var*100, 'b*-')
    plt.grid(True)
    plt.xlabel('Number of clusters')
    plt.ylabel('Percentage of variance explained')
    plt.title('Elbow for KMeans clustering')
    plt.show()

    # elbow curve
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(K, criterion, 'b*-')
    ax.plot(K[kIdx], criterion[kIdx], marker='o', markersize=12, 
    markeredgewidth=2, markeredgecolor='r', markerfacecolor='None')
    plt.grid(True)
    plt.xlabel('Number of clusters')
    plt.ylabel('Average within-cluster sum of squares')
    plt.title('Finding Elbow: Average distance')
    plt.show()

    # percentage variance
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(K, variance, 'b*-')
    plt.grid(True)
    plt.xlabel('Number of clusters')
    plt.ylabel('Percentage of variance')
    plt.title('Finding Elbow: Variance')
    plt.show()

    #Shilhouette
    plt.plot(K, s, 'b*-')
    plt.ylabel("Silouette")
    plt.grid(True)
    plt.xlabel("k")
    plt.title("Finding Elbow: Silouette")
    plt.show()
    #sns.despine() # we dont have this

    #Angles
    plt.plot(K, seg_gains, 'b*-')
    plt.ylabel("Angle between segments (normalized)")
    plt.grid(True)
    plt.xlabel("k")
    plt.title("Finding Elbow: Angles between segments of Cost (i think)")
    plt.show()

def clouds(predicts,k):

    for j in range(0,k):
        data = []
        for i in range(0,len(predicts)):
            if(j==predicts[i]):
                data.append(indexes[i])
        cloud.generateWordCloud(data,currentRun+'-wordCloud'+str(j))

# Main
if __name__ == '__main__':
    costs = {}
    tipo = 'word'
    nsamples = 1
    Y = 10
    M = 300
    n = (1,1)
    currentRun = "-" + str(nsamples) + "samples" + str(n[0]) +  str(n[1]) + str(tipo) + "Max" + str(M) + "year" + str(Y)

    #Test load and plot
    if len(argv) > 1 and argv[1] == "show":
        tipo = 'word'
        print("showing stuff")
        for M in [12000]:
            for n in [(3,3)]:
                currentRun = "-" + str(nsamples) + "samples" + str(n[0]) +  str(n[1]) + str(tipo) + "Max" + str(M) + "year" + str(Y)
                try:
                    loadFullKMeans(currentRun)
                    print(str(M) + str(n))
                except Exception as e:
                    print(e)  
        
        plt.legend(str(M) + str(n))
        plt.show()
        quit()

    # Big test
    #for Y in [4, 10]:
    for M in [12000]:
        for n in [(3,3)]:
            currentRun = "-" + str(nsamples) + "samples" + str(n[0]) +  str(n[1]) + str(tipo) + "Max" + str(M) + "year" + str(Y)
            print("Current run" + currentRun)
            try:
                features = sparse.load_npz(currentRun + "-features.npz")
                indexes = np.load(currentRun + "-indexes.npy")
                print("Loaded features and indexes!")
            except IOError:
                print("Feature not found, calculating...")
                if M is None: #is svd!
                    features, indexes = p.svd_decomposition(200, n, M, tipo, Y, nsamples)
                    np.save(currentRun + "-indexes.npy", indexes)
                else:
                    features, indexes = f.init(n, tipo=tipo, M=M, Y=Y, num_samples=nsamples) 
                    sparse.save_npz(currentRun + "-features.npz", features)
                    np.save(currentRun + "-indexes.npy", indexes)
                print("Features and indexes saved!")
            print("Clustering")
            kMeans(range(2, 30))
    plt.show()
    #kmeansTest(features)
    #loadFullKMeans(currentRun)
    # kMeans(range(10, 12)) #sparse run!
    






