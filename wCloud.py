from subprocess import check_output
from wordcloud import WordCloud, STOPWORDS
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from nltk.stem import PorterStemmer

#mpl.rcParams['figure.figsize']=(8.0,6.0)    #(6.0,4.0)
mpl.rcParams['font.size']=12                #10 
mpl.rcParams['savefig.dpi']=500             #72 
mpl.rcParams['figure.subplot.bottom']=.1 
stopwords = set(STOPWORDS)
ps = PorterStemmer() 

colnames = ['Date', 'Headline']
rawData = pd.read_csv('news_headlines.csv', names=colnames, header=0)
def generateWordCloud(data,name):
    head = ''
    for i in range(0, len(data)):
        head = head+ rawData['Headline'][data[i]]
    wordcloud = WordCloud(
                          background_color='white',
                          stopwords=stopwords,
                          max_words=200,
                          max_font_size=40, 
                          random_state=42,
                          colormap='inferno'
                         ).generate(str(head))

    print(wordcloud)
    fig = plt.figure()
    plt.imshow(wordcloud)
    plt.axis('off')
    #plt.show()
    #fig.savefig(name+".png", dpi=900)

#generateWordCloud(np.arange(0,1000000),'teste')
