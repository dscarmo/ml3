'''
Reads processed headlines and generates features
'''
from nltk import ngrams
import pandas as pd
import json
import numpy as np
import copy
import math
import operator
from operator import itemgetter, attrgetter
from sklearn.feature_extraction.text import TfidfVectorizer
import random
import re
from gensim.models import Word2Vec
import io


def read_head_lines(path):
	'''
	Reads processed headlines
	'''
	colnames = ['Date', 'Headline']
	print("Reading headlines file...")
	hlines = pd.read_csv(path, names=colnames, header=None)
	#print("State of table: ")
	#print(hlines)
	return hlines

def ngramtf(n, save=True, limit=None):
	'''
	DEPRECATED
	Extracts interesting features using ngram and frequency stuff
	So far we have headline term frequency and global term frequency
	'''
	colindex = str(n) + "gram"
	hlines[colindex] = ""
	size = len(hlines['Headline'])
	print("Going over all " + str(size) + " headlines...")
	for index, row in hlines.iterrows():
		i = int(index)
		#if i % 1000 == 0:
			#print(i, end=' ')
		tokenizedhl = str(row['Headline']).split()
		grams = ngrams(tokenizedhl, n)
		termcount = {}
		
		for gram in grams:
			key = ""
			for word in gram:
				key += word  
			
			# Term count for this headline
			if key not in termcount:
				termcount[key] = 0
			termcount[key] += 1

			# Global term count (all headlines)
			if key not in globaltermcount:
				globaltermcount[key] = 0
			globaltermcount[key] += 1

		hlfeat_key = str(index) + "-" + str(row['Date'])
		headline_features[hlfeat_key] = termcount 
		if limit is not None:
			if index > limit:
				break

	if save is True:
		with open(str(n) + "_gram.txt", 'w') as file:
			file.write(json.dumps(headline_features))
		with open(str(n) + "_globalgram.txt", 'w') as file:
			file.write(json.dumps(globaltermcount))

def char4gramtf(n, save=True, limit=None):
	'''
	DEPRECATED
	Extracts interesting features using ngram and frequency stuff
	So far we have headline term frequency and global term frequency
	'''
	colindex = str(n) + "gram"
	hlines[colindex] = ""
	size = len(hlines['Headline'])
	print("Going over all " + str(size) + " headlines...")
	for index, row in hlines.iterrows():
		i = int(index)
		#if i % 1000 == 0:
			#print(i, end=' ')
		tokenizedhl = str(row['Headline'])
		termcount = {}
		
		for i in range(0,len(tokenizedhl)-3):
			key = tokenizedhl[i]+tokenizedhl[i+1]+tokenizedhl[i+2]+tokenizedhl[i+3]
		
			# Term count for this headline
			if key not in termcount:
				termcount[key] = 0
			termcount[key] += 1

			# Global term count (all headlines)
			if key not in globaltermcount:
				globaltermcount[key] = 0
			globaltermcount[key] += 1

		hlfeat_key = str(index) + "-" + str(row['Date'])
		headline_features[hlfeat_key] = termcount 
		if limit is not None:
			if index > limit:
				break

	if save is True:
		with open("char_4gram.txt", 'w') as file:
			file.write(json.dumps(headline_features))
		with open("char_4globalgram.txt", 'w') as file:
			file.write(json.dumps(globaltermcount))

def load_gram_features(n):
	'''
	DEPRECATED
	Loads and decodes json saved feature
	'''
	with open(str(n) + "_gram.txt", 'r') as file:
		headline_features = json.loads(file.read())
	with open(str(n) + "_globalgram.txt", 'r') as file:
		globaltermcount = json.loads(file.read())
	return globaltermcount,headline_features


def create_vector(tipo,data,n,M):
	'''index_tf = list(tf.keys())
	tam_idf = len(globaltermcount)
	print('Numero termos: '+ str(tam_idf))
	vec_features= copy.copy(tf)
	for j in range(0,len(index_tf)):
		headline = np.empty(tam_idf,dtype=np.float16)
		k=0
		for i in range(0,tam_idf):
			if globaltermcount[i][0] in tf[index_tf[j]]:
				headline[i] = idf_order[globaltermcount[i][0]]*tf[index_tf[j]][globaltermcount[i][0]]
			else:
				headline[i] = 0
			#print(vec_features[j,k])
			k=+1
		vec_features[index_tf[j]] = copy.copy(headline)'''

	tfidf = TfidfVectorizer(analyzer=tipo, ngram_range=n,stop_words=None,use_idf=True,max_features=M)

	#tfidf.fit(data)
	#print(data)
	vec_features = tfidf.fit_transform(data,y=None)

	return vec_features


def tfidf(n, save=True, load=False):
	'''
	DEPRECATED
	'''
	print("Max features: " + str(max_features))
	if load is True:
		if max_features >= 100:
			vec_features = np.load("/mnt/0C9C6DBA9C6D9F48/bigdata/ml3feat/"+str(n)+ "gram_" + str(max_features) + "tfidf_features.npy")
		else:
			vec_features = np.load(str(n)+ "gram_" + str(max_features) + "tfidf_features.npy")

		return vec_features

	globaltermcount,headline_features =  load_gram_features(n)
	idf = copy.copy(globaltermcount)
	tf = copy.copy(headline_features)


	#calculation of tf and count of documents with each term
	for i in globaltermcount:
		idf[i] = 0  
	for j in headline_features:
		headline_terms = headline_features[j]
		for k in headline_terms:
			idf[k]+=tf[j][k]
			tf[j][k] = float(tf[j][k])/float(len(headline_terms))

	#calculation of idf
	for i in globaltermcount:
		idf[i] = math.log(1000001.0/idf[i])

	#ordering by higher idf
	globaltermcount =  sorted(globaltermcount.items(), key=operator.itemgetter(1),reverse=True) #erro de sintaxe aqui pra mim

	#features extraction
	vec_features = create_vector(globaltermcount,idf,tf, 0)

	if save is True:
		np.save(str(n)+ "gram_" + str(max_features) + "tfidf_features", vec_features)

	return vec_features


def load_vectors(fname):
	fin = io.open(fname, 'r', encoding='utf-8', newline='\n', errors='ignore')
	n, d = map(int, fin.readline().split())
	data = {}
	for line in fin:
		tokens = line.rstrip().split(' ')
		num_feat = len(tokens)-1
		aux = []
		for i in tokens[1:]:
			aux.append(float(i))
		data[(tokens[0]).lower()] = copy.copy(aux)
	return data, num_feat

def wordEmbedding(data, name_embedding):
	model,num_feat = load_vectors(name_embedding)
	features_vec=[]
	for i in range(0,len(data)):
		data[i] = re.sub('[!@#$.,;'':]', '', data[i])
		aux2 = str(data[i]).split()
		#print(aux2)
		tam=len(aux2)
		aux=[]
		tam2 = 0
		for j in range(0,tam):
			if(aux2[j] in model):
				tam2+=1
				if(aux==[]):
					aux=model[aux2[j]]
				else:
					for k in range(0,num_feat):
						aux[k] = aux[k] + model[aux2[j]][k]
				#print(len(aux))
		if(aux==[]):
			features_vec.append([0 for i in range(0,num_feat)])
		else:
			for k in range(0,num_feat):
				aux[k] = aux[k]/tam2
			features_vec.append(copy.copy(aux))
	#print(features_vec)
	return features_vec



##################### MAIN ##############################
headlinePath = "wordsNSNS.csv"
headline_features = {} # get a headline word count by index-date
globaltermcount = {} # term frequency for whole thing
hlines = None
max_features = 1000 
num_feat = 0


def init(n,tipo='word', words=headlinePath, M=None, Y=2,num_samples=1, name_embedding='wiki-news-300d-1M.vec', feat='gram'):
	'''
	Y é 2 quando deseja-se utilizar todos os anos, 3 apenas para 2003 e assim por diante até 17 para 2017
	tipo é word (para n-gram) ou char (para char-n-gram)
	'''
	global hlines
	hlines = read_head_lines(words)
	head = []
	head2 = []
	count_year = np.zeros(16)

	#print(len(hlines))
	for index, row in hlines.iterrows():
		year = int(str(row['Date'])[2:4])
		count_year[year-2]+=1
		head.append(str(row['Headline']))
	for i in range(1,16):
		count_year[i]+= count_year[i-1]
	#head = (hlines[['Headline']])
	indices = []
	if(Y==2):
		if(num_samples==1):
			indices = np.arange(len(head))
			if feat == 'gram':
				print("using gram feats")
				features_vec = create_vector(tipo,head,n,M)
			else:
				print("using embedings")
				features_vec = wordEmbedding(head, name_embedding)
		else:
			#teste 10% de headlines
			lista_rand = random.sample(range(0,len(head)), int(len(head)*num_samples))
			print("Num rand:" + str(len(lista_rand)))
			for i in range(0, len(lista_rand)):
				head2.append(head[lista_rand[i]]) 
			#####
			print("Num head2:" + str(len(head2)))
			indices = lista_rand
			if feat == 'gram':
				print("using gram feats")
				features_vec = create_vector(tipo,head2,n,M)
			else:
				print("using embedings")
				features_vec = wordEmbedding(head2, name_embedding)


			
	else:
		if feat == 'gram':
				print("using gram feats")
				features_vec = create_vector(tipo,head[int(count_year[Y-3]):int(count_year[Y-2])],n,M)
		else:
			print("using embedings")
			features_vec = wordEmbedding(head[int(count_year[Y-3]):int(count_year[Y-2])], name_embedding)
		indices = range(int(count_year[Y-3]), int(count_year[Y-2]))
	#for i in range(0,features):
	#	if(features_vec[5,i]!=0):
	#		print(features_vec[5,i])
	
	return features_vec, indices

# Debug features (roda isso aqui se quiser gerar feature)
#n = 4
#init(n, loadTfidf=False, extractGram=True, M=max_features)
