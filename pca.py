
import features as f
import numpy as np
from sklearn.decomposition import TruncatedSVD,PCA 
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE

 

def svd_decomposition(n_features,n,M,tipo,Y, nsamples):
	reduction = TruncatedSVD(n_components = n_features)
	features, indexes = f.init(n,tipo=tipo, M=M, Y=Y,num_samples=nsamples)
	features = reduction.fit_transform(features)
	print(features.shape)
	return features, indexes

def t_sne(data,n,M,tipo,Y):
	X_embedded = TSNE(n_components=2).fit_transform(data)
	fig, ax = plt.subplots(figsize=(8, 4))
	ax.scatter(X_embedded[:,0],X_embedded[:,1],c='r',label='Unlabeled')
	ax.grid(True)
	ax.legend(loc='best')
	plt.xlabel("x")
	plt.ylabel("y")
	plt.title("Projection of Extracted Features")
	plt.savefig(str(n)+'_'+tipo+_+str(M)+_+'year_'+str(Y)+'featuresTsne.png') 
	plt.show()


#features = svd_decomposition(100,n,)
#t_sne(features,1,None,'word',2)