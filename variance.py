
from sklearn.metrics import euclidean_distances
import numpy as np
from matplotlib import pyplot as plt

def variancePerCluster(labels, features, centroids):
    '''
    Labels: array of clusters assigned to features
    Features: array of arrays of features
    Centroids: points of clusters
    '''
    k = len(centroids)
    pointsPerCluster = []
    distsPerCluster = []
    variancePerCluster = []
    for _ in range(0, k):
        pointsPerCluster.append([])
        distsPerCluster.append([])
        variancePerCluster.append(0)
    
    for label, feature in zip(labels, features):
        pointsPerCluster[label].append(feature)
    
    for cluster, centroid in enumerate(centroids): #each centroid represents a label from 0 to k
        distsPerCluster[cluster] = euclidean_distances(pointsPerCluster[cluster], [centroid])
    
    for cluster, dists in enumerate(distsPerCluster):
        variancePerCluster[cluster] = np.var(dists)
    
    #plt.figure()
    plt.plot(range(2, k + 2), variancePerCluster, 'b*-')
    plt.title("Variance per Cluster" + str(k))
    plt.show()
    return variancePerCluster

def varianceTest():
    labels = [0, 0, 0, 1, 1, 1, 2, 2, 2]
    features = [[0, 0, 1],
                [0, 1, 0],
                [1, 0, 0],
                [1, 1, 1],
                [1, 1, 1],
                [0, 0, 0],
                [2, 2, 2],
                [3, 3, 3],
                [1, 1, 1]]
    centroids = [[0, 1, 0],
                 [0, 0 ,0],
                 [2, 2, 2]]
    variancePerCluster(labels, features, centroids)

#varianceTest()

